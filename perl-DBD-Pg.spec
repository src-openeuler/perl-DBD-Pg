Name:           perl-DBD-Pg
Version:        3.18.0
Release:        2
Summary:        DBD::Pg-PostgreSQL database driver for the DBI module
License:        GPL-2.0-or-later OR Artistic-1.0-Perl
Source0:        https://cpan.metacpan.org/authors/id/T/TU/TURNSTEP/DBD-Pg-%{version}.tar.gz
URL:            https://metacpan.org/release/DBD-Pg

BuildRequires:  findutils gcc make perl-devel perl-generators perl-interpreter perl(Carp) perl(Config)
BuildRequires:  perl(ExtUtils::MakeMaker) >= 6.76 perl(File::Spec) perl(lib) perl(strict) perl(vars)
BuildRequires:  perl(warnings) postgresql-devel >= 7.4 perl(DBI) >= 1.614 perl(DynaLoader) perl(Exporter)
BuildRequires:  perl(if) perl(version) perl(charnames) perl(constant) perl(Cwd) perl(Data::Dumper) perl(open)
BuildRequires:  perl(POSIX) perl(Test::More) >= 0.88 perl(Test::Simple) perl(Time::HiRes) perl(utf8)
BuildRequires:  postgresql-server perl(Encode) perl(File::Temp)

%description
DBD::Pg-PostgreSQL database driver for the DBI module

%package_help

%prep
%autosetup -n DBD-Pg-%{version} -p1

%build
perl Makefile.PL INSTALLDIRS=vendor OPTIMIZE="$RPM_OPT_FLAGS" NO_PACKLIST=1
%make_build

%install
make pure_install DESTDIR=$RPM_BUILD_ROOT
%{_fixperms} $RPM_BUILD_ROOT/*

%check
make test

%files
%{perl_vendorarch}/DBD/
%{perl_vendorarch}/auto/DBD/
%{perl_vendorarch}/Bundle/DBD/Pg.pm

%files help
%doc Changes README README.dev TODO
%{_mandir}/man3/*.3*

%changelog
* Tue Jan 07 2025 Funda Wang <fundawang@yeah.net> - 3.18.0-2
- cleanup spec

* Thu Apr 18 2024 xiejing <xiejing@kylinos.cn> - 3.18.0-1
- update to 3.18.0

* Thu Mar 28 2024 xiejing <xiejing@kylinos.cn> - 3.16.3-2
- fix words spelling

* Sun Jun 25 2023 dillon chen <dillon.chen@gmail.com> - 3.16.3-1
- update to 3.16.3

* Thu Nov 17 2022 dillon chen <dillon.chen@gmail.com> - 3.14.2-1
- update to 3.14.2

* Fri Apr 24 2020 songzhenyu<songzhenyu@huawei.com> - 3.7.4-6
- Package init
